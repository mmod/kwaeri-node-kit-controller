/**
 * SPDX-PackageName: kwaeri/controller
 * SPDX-PackageVersion: 0.3.4
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES


// DEFINES
declare const render: Function;

export interface ApplicationController {
    index( request: any, response: any ): any;
}


export class BaseController implements ApplicationController {
    render?: Function;

    constructor() {
        if( !this.render )
            this.render = render;
    }

    // HTTP GET <ROUTE>/
    index( request: any, response: any ): void {
        this.render!( request, response );
    }
}